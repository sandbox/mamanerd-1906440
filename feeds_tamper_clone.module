<?php

/**
 * @file
 * Gives user the option to clone tamper plugins when cloning a feed importer.
 */

/**
 * Implements hook_form_BASE_FORM_ID_alter().
 */
function feeds_tamper_clone_form_feeds_ui_create_form_alter(&$form, &$form_state) {

  // Only make these alterations if we're trying to clone a feed.
  if (arg(4) == 'clone') {

    // Add checkbox to clone tamper plugins.
    $form['clone_tamper_plugins'] = array(
      '#type' => 'checkbox',
      '#title' => t('Clone Tamper Plugins'),
      '#description' => t('Check this box if you also want to clone the Feeds Tamper plugins for this importer.'),
      '#weight' => 1,
    );

    // Give submit button a higher weight so it shows up under the checkbox.
    $form['submit']['#weight'] = 2;
    $form['#submit'][] = 'feeds_tamper_clone_clone_tamper_plugins';

    return $form;

  }
}

/**
 * Additional submit handler for feeds_build_create_form().
 */
function feeds_tamper_clone_clone_tamper_plugins($form, &$form_state) {

  if ($form_state['values']['clone_tamper_plugins'] == 1) {

    // Let's get the #from_importer ID.
    $from_importer_id = isset($form['#from_importer']->fetcher->id) ? $form['#from_importer']->fetcher->id : '';

    // Now let's find all the feeds tamper plugins with #from_importer ID.
    if ($from_importer_id) {
      $tamper_plugins = feeds_tamper_load_by_importer($from_importer_id, FALSE);
    }

    // For each tamper plugin found, let's change the #from_importer ID to our
    // newly cloned importer ID. We also need to change some other settings
    // before we can save the new tamper plugin instance.
    foreach ($tamper_plugins as $tamper_plugin) {
      foreach ($tamper_plugin as $old_instance) {
        $new_instance = feeds_tamper_new_instance();
        $new_instance = $old_instance;
        $new_instance->id = str_replace($from_importer_id, $form_state['values']['id'], $new_instance->id);
        $new_instance->importer = $form_state['values']['id'];
        $new_instance->type = 'local';
        $new_instance->export_type = NULL;
        unset($new_instance->table);
        unset($new_instance->disabled);
        feeds_tamper_save_instance($new_instance);
      }
    }

  }

}
