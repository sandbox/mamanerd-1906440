
FEEDS_TAMPER_CLONE
=====

Option to clone Feeds Tamper plugins when cloning a Feeds importer.
http://drupal.org/project/feeds_tamper_clone

Features
========

- Gives users the option to clone Feeds Tamper plugins when cloning a Feeds 
  importer.

Requirements
============

- Drupal 7.x
  http://drupal.org/project/drupal
- Feeds Tamper 7.x
  http://drupal.org/project/feeds_tamper

Installation
============

- Enable Feeds Tamper Clone module.
- Go to your list of Feeds at admin/structure/feeds.
- Next to one of your feeds, click "Clone".
- You should be given the option to clone tamper plugins.
- Click "Create".
